//
//  CustomCell.swift
//  IOASYS
//
//  Created by Miller on 12/09/19.
//  Copyright © 2019 Miller. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet weak var enterpriseName: UILabel!
    @IBOutlet weak var enterpriseType: UILabel!
    @IBOutlet weak var enterpriseCountry: UILabel!
    var enterpriseId: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = CGFloat(10)
    }
}
