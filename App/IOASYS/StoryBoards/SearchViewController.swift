//
//  SearchViewController.swift
//  IOASYS
//
//  Created by Miller on 12/09/19.
//  Copyright © 2019 Miller. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var enterprisesCollectionView: UICollectionView!
    private var searchController: UISearchController?
    private var imageView: UIImageView?
    private let cellId = "CustomCell"
    var enterprises: Array<EnterpriseModel>?
    var filteredEnterprises = Array<EnterpriseModel>()
    private var lastAnimatedCellPosition: Int = -1
    private let defaultImage = UIImage(named: "enterprise_image")
    private let segueEnterpriseViewControllerId = "ShowEnterpriseViewController"
    private var searchButton: UIBarButtonItem?
    private var dataFromApi: Bool = true

    @IBOutlet weak var collectionViewBottonConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupCollectionView()
    }
    
    private func setupNavigationBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchBar.delegate = self
        searchController?.hidesNavigationBarDuringPresentation = false
        imageView = UIImageView(image: UIImage(named: "ioasys_logo_nav"))
        let searchImage = UIImage(named: "search_icon")
        navigationItem.titleView = imageView
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 222/255, green: 71/255, blue: 114/255, alpha: 1.0)
        searchButton = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(setupSearch))
        
        navigationItem.rightBarButtonItems = [searchButton!]
        navigationItem.rightBarButtonItem?.tintColor = .white
        searchController?.dimsBackgroundDuringPresentation = false
    }
    
    @objc func setupSearch() {
        navigationItem.rightBarButtonItems = []
        navigationItem.titleView = searchController?.searchBar
    }
    
    private func setupCollectionView() {
        enterprisesCollectionView.delegate = self
        enterprisesCollectionView.dataSource = self
        
        let nibName = UINib(nibName: cellId, bundle: nil)
        enterprisesCollectionView.register(nibName, forCellWithReuseIdentifier: cellId)
        
        if let notch = UIApplication.shared.keyWindow?.safeAreaInsets.bottom, notch > .zero {
            collectionViewBottonConstraint.constant -= notch
        }
        enterprisesCollectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            let query = text.lowercased()
            let newEnterprises = enterprises?.filter {
                $0.enterprise_name.lowercased().contains(query)
            }
            if !newEnterprises!.isEmpty {
                filteredEnterprises = newEnterprises!
                lastAnimatedCellPosition = -1
                dataFromApi = false
                enterprisesCollectionView.reloadData()
            } else {
                if !filteredEnterprises.isEmpty && enterprises?.count != filteredEnterprises.count {
                    filteredEnterprises = enterprises!
                    lastAnimatedCellPosition = -1
                    enterprisesCollectionView.reloadData()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dataFromApi {
            return enterprises?.count ?? 0
        } else {
            return filteredEnterprises.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let enterprise = dataFromApi ? enterprises![indexPath.item] : filteredEnterprises[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomCell
        cell.enterpriseName.text = enterprise.enterprise_name
        cell.enterpriseType.text = enterprise.enterprise_type.enterprise_type_name
        cell.enterpriseCountry.text = enterprise.country
        cell.enterpriseImage.image = defaultImage
        cell.enterpriseId = enterprise.id
        
        if let imageUrl = enterprise.photo {
            let baseUrl = "http://empresas.ioasys.com.br"
            
            if let url = URL(string:  baseUrl + imageUrl) {
                cell.enterpriseImage.af_setImage(
                    withURL: url,
                    placeholderImage: nil,
                    filter: nil,
                    imageTransition: .crossDissolve(0.2)
                )
            }
        }
        if indexPath.item > self.lastAnimatedCellPosition {
            cell.transform = CGAffineTransform(
                translationX: 0,
                y: 50.0
            )
            cell.alpha = 0.1
            
            UIView.animate(withDuration: 1.0, animations: {
                cell.transform = .identity
                cell.alpha = 1
            })
            self.lastAnimatedCellPosition = indexPath.item
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return .init(width: UIScreen.main.bounds.width, height: CGFloat(122))
    }

    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        UIView.animate(withDuration: 0.3, animations: {
            cell?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        UIView.animate(withDuration: 0.1, animations: {
            cell?.transform = .identity
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CustomCell else {
            return
        }
        if let enterpriseId = cell.enterpriseId {
            Api.shared.fetchEnterpriseById(enterpriseId: enterpriseId) { isSuccessful, enterprise in
                if isSuccessful {
                    let enterpriseViewController = UIStoryboard(name: "Search", bundle: nil)
                    
                    guard let vc = enterpriseViewController.instantiateViewController(withIdentifier: "EnterpriseViewController") as? EnterpriseViewController else {
                        return
                    }
                    vc.enterprise = enterprise
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.navigationItem.titleView = self.imageView
                    self.navigationItem.rightBarButtonItems = [self.searchButton!]
                    
                } else {
                    print("Erro ao buscar informacoes da empresa! ID = ", enterpriseId)
                }
            }
        }
    }
}
