//
//  ViewController.swift
//  IOASYS
//
//  Created by Miller on 11/09/19.
//  Copyright © 2019 Miller. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var btn: UIButton!
    private let segueSearchViewControllerId = "ShowSearchViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn.layer.cornerRadius = CGFloat(6)
    }

    @IBAction func onClickBtn(_ sender: UIButton) {
        let login = emailTextField.text ?? ""
        let senha = passwordTextField.text ?? ""
        
        if login.isEmpty || senha.isEmpty {
            print("Erro durante o login!")
            return
        }
        let api = Api.shared
        
        api.login(userLogin: login, userPass: senha) { isSuccessful in
            if isSuccessful {
                self.openSearchViewController()
            } else {
                print("Erro durante o login!")
            }
        }
    }
    
    func openSearchViewController() {
        Api.shared.fetchEnterprisesByFilter() { enterprises in
            if !enterprises.isEmpty {
                self.performSegue(withIdentifier: self.segueSearchViewControllerId, sender: enterprises)
            } else {
             print("Nao foram retornados empresas.")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navigationViewController = segue.destination as? NavigationViewController {
            if let viewController = navigationViewController.viewControllers.first as? SearchViewController,
                let sentEnterprises = sender as? [EnterpriseModel] {
                viewController.enterprises = sentEnterprises
            }
        }
    }
    @IBAction func onEmailFinish(_ sender: UITextField) {
        emailTextField.resignFirstResponder()
        passwordTextField.becomeFirstResponder()
    }
    
    @IBAction func onPasswordFinish(_ sender: UITextField) {
        if let email = emailTextField.text, let password = sender.text {
            if !email.isEmpty && !password.isEmpty {
                let api = Api.shared
                btn.isEnabled = false
                
                api.login(userLogin: email, userPass: password) { isSuccessful in
                    if isSuccessful {
                        self.openSearchViewController()
                    } else {
                        print("Erro durante o login!")
                    }
                    self.btn.isEnabled = true
                }
            } else {
                print("Erro durante o login!")
            }
        } else {
            print("Erro durante o login!")
        }
    }
}
