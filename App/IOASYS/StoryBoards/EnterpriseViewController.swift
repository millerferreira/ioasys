//
//  EnterpriseViewController.swift
//  IOASYS
//
//  Created by Miller on 12/09/19.
//  Copyright © 2019 Miller. All rights reserved.
//

import UIKit
import AlamofireImage

class EnterpriseViewController: UIViewController {
    
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet weak var textContainerView: UIView!
    @IBOutlet weak var enterpriseDescription: UILabel!
    private let baseUrl = "http://empresas.ioasys.com.br"
    var enterprise: EnterpriseModel?
    let defaultImage = UIImage(named: "enterprise_image")

    override func viewDidLoad() {
        super.viewDidLoad()

        textContainerView.layer.cornerRadius = CGFloat(6)
        
        if let enterprise = enterprise {
            enterpriseImage.image = defaultImage
            enterpriseDescription.text = enterprise.description
            
            if let imageUrl = enterprise.photo {
                if let url = URL(string:  baseUrl + imageUrl) {
                    enterpriseImage.af_setImage(
                        withURL: url,
                        placeholderImage: nil,
                        filter: nil,
                        imageTransition: .crossDissolve(0.2)
                    )
                }
            }
            navigationItem.title = enterprise.enterprise_name
            navigationController?.navigationBar.topItem?.title = ""
            navigationController?.navigationBar.tintColor = .white
        }
    }
}
