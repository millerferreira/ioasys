//
//  Api.swift
//  IOASYS
//
//  Created by Miller on 11/09/19.
//  Copyright © 2019 Miller. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Api {
    
    static let shared = Api()
    private let baseUrl = "http://empresas.ioasys.com.br"
    private let apiVersion = "v1"
    var responseHeaders: [String: String]?
    
    func login(userLogin user: String, userPass password: String, _ onCompletion: ((Bool) -> Void)? = nil) {
        let path = baseUrl + "/api/" + apiVersion + "/users/auth/sign_in"
        
        guard let loginUrl = URL(string: path) else {
            return
        }
        let bodyParams: Parameters = ["email": user, "password": password]
        let headerParams: HTTPHeaders = ["Content-Type": "application/json"]
        
        Alamofire.request(loginUrl, method: .post, parameters: bodyParams, encoding: JSONEncoding.default, headers: headerParams).responseJSON { response in
            
            switch response.result {
            case .success:
                guard response.response?.statusCode == 200, let headers = response.response?.allHeaderFields as? [String: String] else {
                    
                    onCompletion?(false)
                    return
                }
                self.responseHeaders = headers
                onCompletion?(true)
                
            case .failure(let error):
                print(error)
                onCompletion?(false)
            }
        }
    }
    
    func fetchEnterpriseById(enterpriseId id: Int, _ onCompletion: ((Bool, EnterpriseModel?) -> Void)? = nil) {
        guard let path = URL(string: baseUrl + "/api/" + apiVersion + "/enterprises/" + String(id)) else {
            return
        }
        var urlPath = URLRequest(url: path)
        
        guard let loginHeaders = responseHeaders,
            let client = loginHeaders["client"],
            let token = loginHeaders["access-token"],
            let uid = loginHeaders["uid"] else {
            return
        }
        urlPath.addValue(client, forHTTPHeaderField: "client")
        urlPath.addValue(uid, forHTTPHeaderField: "uid")
        urlPath.addValue(token, forHTTPHeaderField: "access-token")
        urlPath.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlPath.httpMethod = HTTPMethod.get.rawValue
        
        Alamofire.request(urlPath).responseJSON { response in
            switch response.result {
            case .success:
                guard let data = response.data else {
                    return
                }
                do {
                    let enterpriseResult = try JSONDecoder().decode(SingleEnterpriseModel.self, from: data)
                    onCompletion?(true, enterpriseResult.enterprise)
                } catch let error {
                    print(error)
                    onCompletion?(false, nil)
                }
            case .failure(let error):
                print(error)
                onCompletion?(false, nil)
            }
        }
    }
    
    func fetchEnterprisesByFilter(enterpriseName name: String? = nil, enterprisesType type: String? = nil, _ onCompletion: (([EnterpriseModel]) -> Void)? = nil) {
        guard let loginHeaders = responseHeaders,
            let client = loginHeaders["client"],
            let token = loginHeaders["access-token"],
            let uid = loginHeaders["uid"] else {
                return
        }
        let path = baseUrl + "/api/" + apiVersion + "/enterprises"
        var urlParams = URLComponents(string: path)
        urlParams?.queryItems = Array<URLQueryItem>()
        
        if let enterpriseType = type {
            urlParams?.queryItems?.append(.init(name: "enterprise_types", value: enterpriseType))
        }
        if let enterpriseName = name {
            urlParams?.queryItems?.append(.init(name: "name", value: enterpriseName))
        }
        guard let url = urlParams?.url else {
            return
        }
        var urlRequest = URLRequest(url: url)        
        urlRequest.addValue(client, forHTTPHeaderField: "client")
        urlRequest.addValue(uid, forHTTPHeaderField: "uid")
        urlRequest.addValue(token, forHTTPHeaderField: "access-token")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        
        Alamofire.request(urlRequest).responseJSON { response in
            switch response.result {
            case .success:
                guard let data = response.data else {
                    return
                }
                do {
                    let enterprisesResult = try JSONDecoder().decode(MultipleEnterprisesModel.self, from: data)
                    onCompletion?(enterprisesResult.enterprises)
                } catch let error {
                    print(error)
                    onCompletion?(Array<EnterpriseModel>())
                }
            case .failure(let error):
                print(error)
                onCompletion?(Array<EnterpriseModel>())
            }
        }
    }
}
