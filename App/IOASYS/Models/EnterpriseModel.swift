//
//  EnterprizeModel.swift
//  IOASYS
//
//  Created by Miller on 11/09/19.
//  Copyright © 2019 Miller. All rights reserved.
//

struct EnterpriseType: Codable {
    var id: Int
    var enterprise_type_name: String?
    
}

struct EnterpriseModel: Codable {
    var id: Int
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool?
    var enterprise_name: String
    var photo: String?
    var description: String
    var city: String
    var country: String
    var value: Int?
    var share_price: Float?
    var shares: Int?
    var own_shares: Int?
    var enterprise_type: EnterpriseType
}

struct MultipleEnterprisesModel: Codable {
    var enterprises: Array<EnterpriseModel>
}

struct SingleEnterpriseModel: Codable {
    var enterprise: EnterpriseModel
}
